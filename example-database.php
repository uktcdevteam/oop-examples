<?php

// We make a new connection to the database
$db = new PDO("mysql:host=localhost;dbname=subd", "root", "");

$stmt = $db->prepare("SELECT * FROM students WHERE id > :firstID AND id < :lastID");
$stmt->execute([
    'firstID' => 2,
    'lastID' => 6
]);

$result = $stmt->fetchAll();

foreach ($result as $key => $value) {
    echo $value['name'] . '<br>';
}